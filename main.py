# run.py

# system imports
import os

# third-party imports

# local imports
from server.app import create

# package code
config_name = os.getenv('FLASK_ENV', 'dev')

app = create(config_name)

if __name__ == '__main__':
    if config_name == 'local':
        app.run(host='0.0.0.0', port=9090, debug=True)
    elif config_name == 'dev':
        app.run(debug=True)
    else:
        app.run()