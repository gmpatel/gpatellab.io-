var signout = new Page("signout");

signout.show = function(page, json) {
    conslog.log("framework-log", ">>", "signout.show()", "page", page, json);
}

signout.performSignout = function (redirectPage) {
    var auth2 = gapi.auth2.getAuthInstance();
    auth2.signOut().then(function () {
        conslog.log("framework-log", ">>", "User signed out successfully!");
        app.deleteUser();
        window.location.replace("/");    
    });
}