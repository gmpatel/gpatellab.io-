var signin = new Page("signin", "splash");

signin.show = function(page, json) {
    conslog.log("framework-log", ">>", "signin.show()", "page", page, json);
    gapi.load('auth2', function() {
        gapi.auth2.init().then(function(auth2) {
            var isSignedIn = auth2.isSignedIn.get();  
            conslog.log("framework-log", ">>", "signin.show", "isSignedIn", isSignedIn);
            if (!isSignedIn) {
                $("#signin-overlay").addClass("hide");
            }
            signin.renderGoogleSigninButton(page.redirect); 
        });
    });
}

signin.renderGoogleSigninButton = function(redirectPage) {
    gapi.signin2.render("google-signin", {
        "onsuccess": function (googleUser) {
            conslog.log("framework-log", ">>", "signin.renderGoogleSigninButton", "onsuccess", googleUser, redirectPage);
            
            app.setUser("Google", googleUser, function() {
                if (!app.user.userAuthorised) {
                    redirectPage = { name: "authorisation" };
                }

                var redirectPath = !redirectPage ? undefined : app.getHashPath(redirectPage);
                conslog.log("framework-log", ">>", "signin.renderGoogleSigninButton", "onsuccess", app.user, redirectPage, redirectPath);
                
                if(redirectPath) {
                    app.loadContent(redirectPath);
                }
            });
        },
        "onfailure": function (error) {
            conslog.error("framework-error", ">>", "signin.renderGoogleSigninButton", "onfailure", "google signin error", error);
        },
        "scope": "profile email",
        "width": 240,
        "height": 50,
        "longtitle": true,
        "theme": "dark"
    });
}

signin.checkAuthorisation = function() {

}