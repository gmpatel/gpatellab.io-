var pnf = new Page("pnf", "splash");

pnf.show = function(page, json) {
    conslog.log("framework-log", ">>", "pnf.show()", "called", page, json);
};

pnf.navigateHome = function() {
    window.location.hash = "#home";
}