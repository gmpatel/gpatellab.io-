var authorisation = new Page("authorisation", "splash");

authorisation.show = function(page, json) {
    conslog.log("framework-log", ">>", "authorisation.show()", "page", page, json);
};

authorisation.performSignout = function (redirectPage) {
    var auth2 = gapi.auth2.getAuthInstance();
    auth2.signOut().then(function () {
        conslog.log("framework-log", ">>", "User signed out successfully!");
        app.deleteUser();
        window.location.replace("/");    
    });
}