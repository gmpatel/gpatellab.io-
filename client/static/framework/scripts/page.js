/* ===================================================================== */
class Page {
    constructor(nameName, type) {
        var dt = type ? type : "page";
        this.page = { name: nameName, displayType: (type ? type : "page")};
    }
    
    navigate(page, json) {
        conslog.log("framework-log", ">>", "page.navigate()", "CLASS", "called", page, json);
        if (app.pageNavigated && typeof(app.pageNavigated) == 'function') {
            conslog.log("framework-log", ">>", "app.pageNavigated", "calling...")
            app.pageNavigated(page, json);
            conslog.log("framework-log", ">>", "app.pageNavigated", "called...")
        }
        this.show(page, json);
    }

    /* show(page) >> Override this method to implement your own page specific navigation code! */
    show(page, json) {
        conslog.warn("framework-warn", ">>", "page.show()", "CLASS", "custom 'show()' page specific method not implemented in page '" + this.page.name + "', trigerred by abscract navigate() method!", page, json);
    }

    getPageName() {
        return this.page.name;
    }

    getDisplayType() {
        return this.page.displayType;
    }
}
/* ===================================================================== */