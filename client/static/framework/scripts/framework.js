/* ===================================================================== */
var app = {}

app.localStorageKey = "gp-app-storage-eed16acd-4d99-4044-9b80-260b11d971f8";
app.localStoreUserObjectName = "user";
app.localStoreAuthenticatedUserPropertyName = "authenticated";
app.skipHashChange = false;

app.pagesRequireAuthentication = ["signout", "authorisation"];
app.authorisedDomains = [];
app.assets = undefined;

app.registerPagesForAuthenticatedAccess = function(pages) {
    if (pages && pages.length > 0) {
        for (i=0; i<pages.length; i++) {
            app.registerPageForAuthenticatedAccess(pages[i]);
        }
    }
};

app.registerPageForAuthenticatedAccess = function(page) {
    if (page) {
        if (page.name) {
            app.pagesRequireAuthentication.push(page.name.toLowerCase());
        } else if (typeof page === "string") {
            app.pagesRequireAuthentication.push(page.toLowerCase());
        }
    }
};

app.setTheme = function(user) {
    if (app.authorisedDomains && app.authorisedDomains.length > 0) {
        for (i = 0; i < app.authorisedDomains.length; i++) {
            if (app.authorisedDomains[i].domain === user.domain) {
                if (app.authorisedDomains[i].themeCss) {
                    var themeCss = app.authorisedDomains[i].themeCss;
                    var cssElement = document.createElement('link');
                    cssElement.rel="stylesheet";
                    cssElement.type="text/css";
                    cssElement.href = themeCss;
                    document.head.appendChild(cssElement);
                }
                break;
            }
        }
    }
};

app.checkUserDomainAuthorised = function(domain) {
    if (app.authorisedDomains && app.authorisedDomains.length > 0) {
        for (i = 0; i < app.authorisedDomains.length; i++) {
            if (app.authorisedDomains[i].domain.toLowerCase() === domain.toLowerCase()) {
                return true;
            }
        }
        return false;
    } else {
        return true;
    }
}

app.checkUserAuthorised = function(domain, email) {
    if (app.authorisedDomains && app.authorisedDomains.length > 0) {
        for (i = 0; i < app.authorisedDomains.length; i++) {
            if (app.authorisedDomains[i].domain.toLowerCase() === domain.toLowerCase()) {
                if(app.authorisedDomains[i].users && app.authorisedDomains[i].users.length > 0) {
                    for (j = 0; j < app.authorisedDomains[i].users.length; j++) {
                        if (app.authorisedDomains[i].users[j].toLowerCase() === email.toLowerCase()) {
                            return true;
                        }
                    }
                    return false;
                } else {
                    return true;
                }
            }
        }
        return false;
    } else {
        return true;
    }
}

app.setUser = function(type, userObject, callback){
    conslog.log("framework-log", ">>", userObject);
                
    var profile = userObject.getBasicProfile();
    var token = userObject.getAuthResponse().id_token;
    var id = profile.getId();
    var name =  profile.getName();
    var givenName = profile.getGivenName();
    var familyName = profile.getFamilyName();
    var imageUrl = profile.getImageUrl();
    var email = profile.getEmail();
    var domain = email.split("@")[1].toLowerCase();
    var domainAuthorised = app.checkUserDomainAuthorised(domain);
    var userAuthorised = app.checkUserAuthorised(domain, email);

    app.user = {
        type: type,
        profile: profile,
        token: token,
        id: id,
        name: name,
        givenName: givenName,
        familyName: familyName,
        imageUrl: imageUrl,
        email: email,
        domain: domain,
        domainAuthorised: domainAuthorised,
        userAuthorised: userAuthorised
    };

    $("#app-avatar").attr("src", imageUrl);
    $("#app-avatar-link").attr("href", '#signout');

    app.setTheme(app.user);

    var json = JSON.stringify(app.user);
    var data = app.encodeBase64(json);
    app.saveSetting(
        app.localStoreUserObjectName, 
        app.localStoreAuthenticatedUserPropertyName, 
        data
    );

    conslog.log("framework-log", ">>", app.user, json, data);
    if (callback && typeof(callback) == "function") {
        callback();
    }
}

app.deleteUser = function(callback){
    app.user = undefined;
    
    $("#app-avatar").attr("src", $("#app-avatar").attr("alt"));
    $("#app-avatar-link").attr("href", "#signin");

    app.removeSetting(
        app.localStoreUserObjectName, 
        app.localStoreAuthenticatedUserPropertyName
    );
}

app.onHashChange = function() {
    if (!app.skipHashChange) {
        app.loadContent();
    }
};

app.loadContent = function(actionPath) {
    var loaderId = "#app-loader"
    $(loaderId).removeClass("hide")
    
    app.processActionPath(actionPath, function(page) {
        conslog.log("framework-log", ">>", "app.loadContent", "after processActionPath()", page);
        app.renderContent("page", page, function(html, meta, page, json) {
            conslog.log("framework-log", ">>", "app.loadContent", "after renderContent()", (html ? "<html> present" : html), page);
            app.viewPageContent(html, meta, page, json);
        });
    });
};

app.processActionPath = function(actionPath, callback) {
    var defaultPageName = "home";
    var signinPageName = "signin";
    var authorisationPageName = "authorisation";
    
    conslog.log("framework-log", ">>", "app.processActionPath", "window.location.hash", window.location.hash, "actionPath", actionPath)
    
    if(actionPath) {
        app.skipHashChange = true;
        window.location.hash = actionPath;
    }

    var urlPath = window.location.hash;
    var url = urlPath.split("/");

    var given = {};
    given.name = url.length >= 1 
        ? url[0].substring(1).toLowerCase() 
        : undefined;
    given.name = given.name === ""
        ? defaultPageName
        : given.name;
    given.action = action = url.length >= 2 
        ? url[1].toLowerCase() 
        : undefined;
    given.params = url.length >= 3 
        ? url.slice(2) 
        : undefined;

    var page = {};
    if (!app.user && app.checkPageForAuthRequired(given)) {
        page.name = signinPageName;
        if(given.name === signinPageName) {
            given.name = defaultPageName;
        }
        page.redirect = given;
    } else if (app.user && !app.user.userAuthorised && app.checkPageForAuthRequired(given)) {
        page.name = authorisationPageName;
        if(given.name === authorisationPageName) {
            given.name = authorisationPageName;
        }
        page.redirect = undefined;
    } else {
        page = given;
    }

    conslog.log("framework-log", ">>", "app.processActionPath", "hash-parsing", url, page);
    if (callback && typeof(callback) == "function") {
        callback(page);
    }
};

app.checkPageForAuthRequired = function(given) {
    if (!app.pagesRequireAuthentication) {
        return false;
    } else if (app.pagesRequireAuthentication.indexOf("*") > -1) {
        return true;
    } else if (app.pagesRequireAuthentication.indexOf(given.name) > -1) {
        return true;
    } else {
        return false;
    } 
};

app.renderView = function(append, viewName, viewContainerId, callback) {
    conslog.log("framework-log", ">>", "app.renderView", append, viewName, viewContainerId);
    app.renderContent("view", {name: viewName}, function(html, meta, pov, json){
        if (append) {
            $(viewContainerId).append(html);
        } else {
            $(viewContainerId).html(html);
        }
        
        var viewNavigateFunctionCall = viewName.replace(/-/g, '_') + ".navigate(json);"
        conslog.log("framework-log", ">>", "app.renderView", "call-respective-view-navigate-func", viewNavigateFunctionCall, viewName, meta, pov, json);
    
        try {
            eval(viewNavigateFunctionCall);
        }
        catch(err) {
            conslog.error("framework-error", ">>", "app.renderView", "viewNavigateFunctionCall", viewNavigateFunctionCall, err);
        }

        if (callback && typeof(callback) == "function") {
            callback();
        }
    });
};

app.renderContent = function(type, pov, callback) {
    var templatesCacheKey = "templates";
    var staticContentsCacheKey = "statics";
    var metaContentsCacheKey = "meta";
    setTimeout(() => {
        app.getCache(templatesCacheKey, type + "-" + pov.name, function(compiledTemplate) {
            if (!compiledTemplate) {
                app.getContents(type, pov, function(template, css, json, meta) {
                    var templateCss = (css ? "<style>\n" + css  + "\n</style>\n" : "") + (template ? template : "");        
                    compiledTemplate = Handlebars.compile(templateCss);
                    app.saveCache(templatesCacheKey, type + "-" + pov.name, compiledTemplate);
                    app.saveCache(staticContentsCacheKey, type + "-" + pov.name, (json ? json : {}));
                    app.saveCache(metaContentsCacheKey, type + "-" + pov.name, (meta ? meta : {}));
                    app.getHtml(pov, compiledTemplate, json, function(html, contents) {
                        if (callback && typeof(callback) == "function") {
                            callback(html, meta, pov, contents);                        
                        }
                    });
                });
            } else {
                conslog.log("framework-log", ">>", "app.renderContent", "compiledTemplate | cache", compiledTemplate);
                app.getCache(staticContentsCacheKey, type + "-" + pov.name, function(json) {
                    app.processJson(type, pov, json, function() {
                        app.getCache(metaContentsCacheKey, type + "-" + pov.name, function(meta) {
                            app.getHtml(pov, compiledTemplate, json, function(html, contents) {
                                if (callback && typeof(callback) == "function") {
                                    callback(html, meta, pov, contents);                        
                                }
                            })
                        });
                    })
                });
            }
        });
    }, 50);
};

app.cyrb53 = function(str, seed = 21) {
    var h1 = 0xdeadbeef ^ seed, h2 = 0x41c6ce57 ^ seed;
    for (var i = 0, ch; i < str.length; i++) {
        ch = str.charCodeAt(i);
        h1 = Math.imul(h1 ^ ch, 2654435761);
        h2 = Math.imul(h2 ^ ch, 1597334677);
    }
    h1 = Math.imul(h1 ^ h1>>>16, 2246822507) ^ Math.imul(h2 ^ h2>>>13, 3266489909);
    h2 = Math.imul(h2 ^ h2>>>16, 2246822507) ^ Math.imul(h1 ^ h1>>>13, 3266489909);
    return 4294967296 * (2097151 & h2) + (h1>>>0);
};

app.setupSidePane = function(open) {
    var ismobile = app.mobileCheck();
    var sidepane = new OSREC.superslide(
    {
        content: 					document.getElementById("app-page"),
        slider: 					document.getElementById("app-pane"),
        animation: 					"slideLeft",
        duration: 					0.2,
        allowDrag: 					true,
        slideContent: 				ismobile ? true : true,
        allowContentInteraction:	true,
        closeOnBlur:				true,
        width: 						ismobile ? "85vw" : "375px",
        height:						"50vh"
    });

    conslog.log("framework-log", ">>", sidepane);
    app.sidepane = sidepane;

    if (open){
        sidepane.open();
        setTimeout(function(){
            sidepane.close();
        }, 1500);
    }
}

app.getHtml = function(pov, compiledTemplate, json, callback) {
    var contents = {};            
    contents.static = json ? json : {};    
    contents = app.appendAppDataContents(contents);

    var html = compiledTemplate(contents);
    conslog.log("framework-log", ">>", "app.getHtml", compiledTemplate, json, contents, (html ? "<html> present" : html));

    if (callback && typeof(callback) == "function") {
        callback(html, contents);                        
    } 
}

app.processJson = function(type, pov, json, callback){
    json.pov = pov ? pov : {};
    conslog.log("framework-log", ">>", "app.processJson", type, pov, json);
    
    if (type === "page" && pov.name === "app") {
        if (json.authorisedDomains) {
            conslog.log("framework-log", ">>", "app.processJson", "page: app", json.authorisedDomains);
            app.authorisedDomains = json.authorisedDomains;    
        }
        if (json.assets) {
            app.assets = json.assets;
        }
    }
    if (json.calls) {
        var triggered = 0;
        var completed = 0;

        for (var key in json.calls) {
           if (((!json.calls[key].level) || (json.calls[key].level === "action" && pov.action) || ((json.calls[key].level === "page" || json.calls[key].level === "view") && !pov.action))) {
                triggered++;
                conslog.log("framework-log", ">>", "app.processJson", "ajax calls triggered", triggered, "ajax calls completed", completed);
                $.ajax({
                    key: key,
                    data: json.calls[key].data,
                    method: json.calls[key].method,
                    async: true,
                    url: json.calls[key].url.replace(/{{action}}/g, pov.action),
                    cache: false,
                    success: function(result) {
                        json.calls[this.key].result = result;
                        json.calls[this.key].err = undefined;
                    },
                    error: function(request, status, err) {
                        json.calls[this.key].result = undefined;
                        json.calls[this.key].err = err;
                    },
                    complete: function(xhr, status) {
                        completed++;
                        conslog.log("framework-log", ">>", "app.processJson", "ajax calls triggered", triggered, "ajax calls completed", completed);
                        
                        json.calls[this.key].status = status + " (" + xhr.status + ")";
                        
                        if (triggered == completed) {
                            conslog.log("framework-log", ">>", "app.processJson", type, pov, json);
                            if (callback && typeof(callback) == "function") {
                                callback(json);
                            }
                        }
                    }   
                });
            }
        }
    } else {
        if (callback && typeof(callback) == "function") {
            callback(json);
        }    
    }
}

app.getContents = function(type, pov, callback) {
    conslog.log("framework-log", ">>", "app.getContents", type, pov);

    var defaultAppPath = "/static/app/";
    var defaultAppPovPath = (type === "page" ? "/static/app/pages/" : "/static/app/views/");
    var defaultFrameworkPath = "/static/framework/";
    var defaultFrameworkPovPath = (type === "page" ? "/static/framework/root/" : "/static/framework/root-views/");
    var defaultTemplatesExtension = ".handlebars";
    var defaultCssExtension = ".css";
    var defaultJsonExtension = ".json";
    var defaultJsExtension = ".js";
    var defaultMetaExtension = ".meta.json";
    var cacheEnabled = false;

    if (pov.name) {
        var templateAppUrl = defaultAppPovPath + pov.name + "/" + pov.name + defaultTemplatesExtension;
        $.ajax({
            method: 'GET',
            async: true,
            url: templateAppUrl,
            cache: cacheEnabled,
            success: function(template) {
                var cssAppUrl = defaultAppPovPath + pov.name + "/" + pov.name + defaultCssExtension;
                var jsonAppUrl = defaultAppPovPath + pov.name + "/" + pov.name + defaultJsonExtension;
                var jsAppUrl = defaultAppPovPath + pov.name + "/" + pov.name + defaultJsExtension;
                
                var jsElement = document.createElement('script');
                jsElement.src = jsAppUrl;
                document.head.appendChild(jsElement);
                
                $.ajax({
                    method: 'GET',
                    async: true,
                    url: cssAppUrl,
                    cache: cacheEnabled,
                    success: function(css) {
                        $.ajax({
                            method: 'GET',
                            async: true,
                            url: jsonAppUrl,
                            cache: cacheEnabled,
                            success: function(json) {
                                app.processJson(type, pov, json, function() {
                                    if (callback && typeof(callback) == "function") {
                                        callback(template, css, json, undefined);
                                    }
                                });
                            },
                            error: function(request, status, err) {
                                if (callback && typeof(callback) == "function") {
                                    callback(template, css, undefined, undefined);
                                }
                            }   
                        });          
                    },
                    error: function(request, status, err) { 
                        $.ajax({
                            method: 'GET',
                            async: true,
                            url: jsonAppUrl,
                            cache: cacheEnabled,
                            success: function(json) {
                                app.processJson(type, pov, json, function() {
                                    if (callback && typeof(callback) == "function") {
                                        callback(template, undefined, json, undefined);
                                    }    
                                });
                            },
                            error: function(request, status, err) { 
                                if (callback && typeof(callback) == "function") {
                                    callback(template, undefined, undefined, undefined);
                                }
                            }   
                        });
                    }   
                });
            },
            error: function(request, status, err) { 
                var templateRootUrl = defaultFrameworkPovPath + pov.name + "/" + pov.name + defaultTemplatesExtension;
                $.ajax({
                    method: 'GET',
                    async: true,
                    url: templateRootUrl,
                    cache: cacheEnabled,
                    success: function(template) {
                        var cssRootUrl = defaultFrameworkPovPath + pov.name + "/" + pov.name + defaultCssExtension;
                        var jsonRootUrl = defaultFrameworkPovPath + pov.name + "/" + pov.name + defaultJsonExtension;
                        var jsRootUrl = defaultFrameworkPovPath + pov.name + "/" + pov.name + defaultJsExtension;
                        
                        var jsElement = document.createElement('script');
                        jsElement.src = jsRootUrl;
                        document.head.appendChild(jsElement);

                        $.ajax({
                            method: 'GET',
                            async: true,
                            url: cssRootUrl,
                            cache: cacheEnabled,
                            success: function(css) {
                                $.ajax({
                                    method: 'GET',
                                    async: true,
                                    url: jsonRootUrl,
                                    cache: cacheEnabled,
                                    success: function(json) {
                                        app.processJson(type, pov, json, function() {
                                            if (callback && typeof(callback) == "function") {
                                                callback(template, css, json, undefined);
                                            }
                                        });
                                    },
                                    error: function(request, status, err) {
                                        if (callback && typeof(callback) == "function") {
                                            callback(template, css, undefined, undefined);
                                        }
                                    }   
                                });          
                            },
                            error: function(request, status, err) { 
                                $.ajax({
                                    method: 'GET',
                                    async: true,
                                    url: jsonRootUrl,
                                    cache: cacheEnabled,
                                    success: function(json) {
                                        app.processJson(type, pov, json, function() {
                                            if (callback && typeof(callback) == "function") {
                                                callback(template, undefined, json, undefined);
                                            }
                                        });
                                    },
                                    error: function(request, status, err) { 
                                        if (callback && typeof(callback) == "function") {
                                            callback(template, undefined, undefined, undefined);
                                        }
                                    }   
                                });
                            }   
                        });
                    },
                    error: function(request, status, err) { 
                        app.renderContent("page", {name: "pnf"}, function(html, meta, pov, json) {
                            conslog.log("framework-log", ">>", "app.loadContent", "after renderContent()", (html ? "<html> present" : html), pov);
                            app.viewPageContent(html, undefined, pov, json);
                        });
                    }   
                });
            }   
        });
    }
}

app.viewPageContent = function(html, meta, page, json) {
    var loaderId = "#app-loader";
    var contentContainerId = "#app-container";
    var splashContainerId = "#app-splash";
    var paneContainerId = "#app-pane";
    var pageContainerId = "#app-page";
        
    app.processMeta(meta, function() {
        var displayType = "splash";
        try {
            if (page && page.name) {
                var getDisplayTypeFunctionCall = page.name + ".getDisplayType();";
                displayType = eval(getDisplayTypeFunctionCall);    
            }
        }
        catch(err) {
            conslog.error("framework-error", ">>", "app.viewPageContent", "getDisplayTypeFunctionCall", getDisplayTypeFunctionCall, err);
        }

        if (displayType === "splash"){
            // Hide app elements
            $(pageContainerId).hide();
            $(paneContainerId).hide();
            $(contentContainerId).hide();
            $(contentContainerId).html("");

            // Display splash elements
            $(splashContainerId).show();
            
            // Inject HTML contents to the splash element
            $(splashContainerId).html(html);
            
            conslog.log("framework-log", ">>", "app.loadContent", "apply-content", displayType, splashContainerId);
        } else {
            // Hide splash elements
            $(splashContainerId).hide();
            $(splashContainerId).html("");
            
            // Display app elements
            $(contentContainerId).show();
            $(pageContainerId).show();
            $(paneContainerId).show();
                        
            // Inject HTML contents to the app pate element with animation
            $(contentContainerId).animate({'opacity': 0}, 250, function(){
                $(contentContainerId).html(html).animate({'opacity': 1}, 150);    
            });
            
            conslog.log("framework-log", ">>", "app.loadContent", "apply-content", displayType, contentContainerId);
        }
        
        $(loaderId).addClass("hide")
        conslog.log("framework-log", ">>", "app.loadContent", "hide-loader", loaderId);
    
        var pageNavigateFunctionCall = page.name.replace(/-/g, '_') + ".navigate(page, json);"
        conslog.log("framework-log", ">>", "app.viewPageContent", "call-respective-page-navigate-func", pageNavigateFunctionCall, page, json);
    
        try {
            eval(pageNavigateFunctionCall);
        }
        catch(err) {
            conslog.error("framework-error", ">>", "app.viewPageContent", "pageNavigateFunctionCall", pageNavigateFunctionCall, err);
        }
    
        if (app.skipHashChange) {
            app.skipHashChange = false;
        }
    });
};

app.processMeta = function(meta, callback) {
    conslog.log("framework-log", ">>", "app.processMeta", "called", meta);

    if(meta) {
        if(meta.title) {
            document.title = meta.title;
        }
    
        if (meta.metaTags && meta.metaTags.length > 0) {
            for (i = 0; i < meta.length; i++) {
                var metaToProcess = meta[i];
                if (metaToProcess && metaToProcess.length > 0) {
                    var meta = $('meta[' + metaToProcess[i].propertyName + '="' + metaToProcess[i].propertyValue + '"]');
                    conslog.log("framework-log", ">>", "app.processMeta", "meta found", meta);
                    
                    if(!meta) {
                        meta = document.head.createElement('meta');
                    }
                            
                    for (i = 0; i < metaToProcess.length; i++) {
                        var property = metaToProcess[i];
                        if (property) {
                            eval('meta.' + property.propertyName + ' = "' + property.propertyValue + '"')
                            
                        }
                    }

                    document.getElementsByTagName("head")[0].appendChild(meta);
                }
            }
        }
    }

    if (callback && typeof(callback) == "function") {
        callback();
    }
}

app.appendAppDataContents = function(contents) {
    contents.appdata = {};

    if (app.user) {
        contents.appdata.user = app.user;
    }
    if (app.authorisedDomains) {
        contents.appdata.authorisedDomains = app.authorisedDomains;
    }

    if (app.assets) {
        contents.appdata.assets = app.assets;
    }
    
    return contents;
};

/* ===================================================================== */
app.mobileCheck = function() {
    var check = false;
    (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor||window.opera);
    return check;
};

app.geoCapable = function() {
    if (navigator.geolocation) {
        conslog.log("framework-log", ">>", 'Geolocation is supported!');
        return true;
    }
    else {
        conslog.log("framework-log", ">>", 'Geolocation is not supported for this Browser/OS.');
        return false;
    }    
}

app.getLocation = function(callback) {
    var supported = app.geoCapable();

    var geoSuccess = function(position) {
        conslog.log("framework-log", ">>", position);

        app.position = position;
        app.latitude = position.coords.latitude;
        app.longitude = position.coords.longitude;
        
        callback(position, position.coords.latitude, position.coords.longitude);
    };

    if (supported) {
        navigator.geolocation.getCurrentPosition(geoSuccess);
    }
}

app.decodeBase64 = function (str, encoding = 'utf-8') {
    var bytes = base64js.toByteArray(str);
    return new(TextDecoder || TextDecoderLite)(encoding).decode(bytes);
}

app.encodeBase64 = function (str, encoding = 'utf-8') {
    var bytes = new(TextEncoder || TextEncoderLite)(encoding).encode(str);
    return base64js.fromByteArray(bytes);
}

/* ===================================================================== */
/* App Cache Methods : Cache Data Client Side For Fast Load              */
/* ===================================================================== */
app.initCache = function(preCachePages) {
    app.cache = [];
    conslog.log("framework-log", ">>", "app.initCache", "app-cache | no-cache", app.cache);
    if(preCachePages) {
        // app.renderContent()
    }
};

app.clearCache = function() {
    app.cache = [];
    conslog.log("framework-log", ">>", "app.clearCache", "app-cache | clear", app.cache);
};

app.getCache = function(objectKey, propertyName, callback) {
    var ret = undefined;
    for (i=0; i < app.cache.length; i ++) {
        if (app.cache[i].key === objectKey) {
            for (p=0; p < app.cache[i].properties.length; p ++) {
                if (app.cache[i].properties[p].name === propertyName) {
                    var value = app.cache[i].properties[p].value;
                    conslog.log("framework-log", ">>", "app.getCache", objectKey, propertyName, value);
                    
                    if (value === "nil") {
                        return ret;
                    }
                    ret = value;

                    if (callback) callback(ret);
                    return ret;
                }
            }
            break;
        }
    }

    if (callback && typeof(callback) == "function") {
        callback(ret);
    }

    return ret;
};

app.getCaches = function(objectKey) {
    for (i=0; i < app.cache.length; i ++) {
        if (app.cache[i].key === objectKey) {
            return app.cache[i].properties;
        }
    }
    return undefined
};

app.removeCache = function(objectKey, propertyName) {
    for (i=0; i < app.cache.length; i ++) {
        if (app.cache[i].key === objectKey) {
            for (p=0; p < app.cache[i].properties.length; p ++) {
                if (app.cache[i].properties[p].name === propertyName) {
                    app.cache[i].properties[p].value = "nil";
                    break;
                }
            }
            break;
        }
    }
    conslog.log("framework-log", ">>", "app.removeCache", "app-cache | remove", objectKey, propertyName, app.cache);
};

app.saveCache = function(objectKey, propertyName, propertyValue) {
    var objectFound = false;
    for (i=0; i < app.cache.length; i ++) { 
        if (app.cache[i].key === objectKey) {
            var propertyFound = false;
            objectFound = true;
            
            for (p=0; p < app.cache[i].properties.length; p ++) {
                if (app.cache[i].properties[p].name === propertyName) {
                    propertyFound = true;
                    app.cache[i].properties[p].value = propertyValue;
                    break;
                }
            }

            if (!propertyFound) {
                app.cache[i].properties.push({
                    name: propertyName,
                    value: propertyValue
                });
            }
            
            break;
        }
    }

    if (!objectFound) {
        app.cache.push({
            key: objectKey,
            properties: [{
                name: propertyName,
                value: propertyValue
            }]
        });
    }
    
    conslog.log("framework-log", ">>", "app.saveCache", "app-cache | save", objectKey, propertyName, propertyValue, app.cache);
};

app.getHashPath = function(page) {
    var path = page.name;

    if (page.action) {
        path = path + "/" + page.action;
    }

    if (page.params) {
        for (i = 0; i < page.params.length; i++) { 
            path = path + "/" + page.params[i];
        }
    }

    conslog.log("framework-log", ">>", "app.getHashPath", page, path);
    return path;    
};

app.getPath = function(page) {
    var path = page.name;

    if (page.action) {
        path = path + "/#" + page.action;
    }

    if (page.params) {
        for (i = 0; i < page.params.length; i++) { 
            path = path + "/" + page.params[i];
        }
    }

    conslog.log("framework-log", ">>", "app.getHashPath", page, path);
    return path;    
};
/* ===================================================================== */

/* ===================================================================== */
/* App Settings Methods : Save App Settings To Browser Local Storage     */
/* ===================================================================== */
app.initSettings = function() {
    if (localStorage[app.localStorageKey]) {
        app.storage = JSON.parse(localStorage[app.localStorageKey]);
        conslog.log("framework-log", ">>", "app.initSettings", "app-storage | cached", app.storage);
    } 
    else {
        app.storage = [];
        conslog.log("framework-log", ">>", "app.initSettings", "app-storage | no-cache", app.storage);   
    }
};

app.clearSettings = function() {
    app.storage = [];
    localStorage[app.localStorageKey] = JSON.stringify(app.storage); //localStorage.removeItem(app.localStorageKey);
    conslog.log("framework-log", ">>", "app.clearSettings", "app-storage | clear", app.storage);
};

app.getSetting = function(objectKey, propertyName) {
    for (i=0; i < app.storage.length; i ++) {
        if (app.storage[i].key === objectKey) {
            for (p=0; p < app.storage[i].properties.length; p ++) {
                if (app.storage[i].properties[p].name === propertyName) {
                    var value = app.storage[i].properties[p].value;
                    conslog.log("framework-log", ">>", "app.getSetting", objectKey, propertyName, value);
                    
                    if (value === "nil") {
                        return undefined;
                    }
                    return value;
                }
            }
            break;
        }
    }
    return undefined
};

app.getSettings = function(objectKey) {
    for (i=0; i < app.storage.length; i ++) {
        if (app.storage[i].key === objectKey) {
            return app.storage[i].properties;
        }
    }
    return undefined
};

app.removeSetting = function(objectKey, propertyName) {
    for (i=0; i < app.storage.length; i ++) {
        if (app.storage[i].key === objectKey) {
            for (p=0; p < app.storage[i].properties.length; p ++) {
                if (app.storage[i].properties[p].name === propertyName) {
                    app.storage[i].properties[p].value = "nil";
                    localStorage[app.localStorageKey] = JSON.stringify(app.storage);
                    break;
                }
            }
            break;
        }
    }
    conslog.log("framework-log", ">>", "app.removeSetting", "app-storage | remove", objectKey, propertyName, app.storage);
};

app.saveSetting = function(objectKey, propertyName, propertyValue) {
    var objectFound = false;
    for (i=0; i < app.storage.length; i ++) { 
        if (app.storage[i].key === objectKey) {
            var propertyFound = false;
            objectFound = true;
            
            for (p=0; p < app.storage[i].properties.length; p ++) {
                if (app.storage[i].properties[p].name === propertyName) {
                    propertyFound = true;
                    app.storage[i].properties[p].value = propertyValue;
                    break;
                }
            }

            if (!propertyFound) {
                app.storage[i].properties.push({
                    name: propertyName,
                    value: propertyValue
                });
            }
            
            break;
        }
    }

    if (!objectFound) {
        app.storage.push({
            key: objectKey,
            properties: [{
                name: propertyName,
                value: propertyValue
            }]
        });
    }
    
    localStorage[app.localStorageKey] = JSON.stringify(app.storage);
    conslog.log("framework-log", ">>", "app.saveSetting", "app-storage | save", objectKey, propertyName, propertyValue, app.storage);
};
/* ===================================================================== */








/* ===================================================================== */
var conslog = {}

conslog.enabled = false;
conslog.level = 0;

conslog.log = function(){}; //console.log.bind(window.console)
conslog.error = function(){}; //console.error.bind(window.console);
conslog.warn = function(){}; //console.warn.bind(window.console);
conslog.debug = function(){}; //console.debug.bind(window.console);
conslog.info = function(){}; //console.info.bind(window.console);

conslog.enable = function() {
    conslog.activate(true);
}

conslog.disable = function() {
    conslog.activate(false);
}

conslog.partial = function(level) {
    conslog.activate(false, level);
}

conslog.activate = function(shouldActivate, partial) {
    conslog.enabled = shouldActivate;
    
    if (shouldActivate) {
        conslog.level = undefined;
        conslog.log = console.log.bind(window.console);
        conslog.error = console.error.bind(window.console);
        conslog.warn = console.warn.bind(window.console);;
        conslog.debug = console.debug.bind(window.console);
        conslog.info = console.info.bind(window.console);
        return;
    } 
    
    conslog.log = function(){}; //console.log.bind(window.console)
    conslog.error = function(){}; //console.error.bind(window.console);
    conslog.warn = function(){}; //console.warn.bind(window.console);
    conslog.debug = function(){}; //console.debug.bind(window.console);
    conslog.info = function(){}; //console.info.bind(window.console);   

    if (partial) {
        conslog.level = partial;

        if (partial == 5) {
            conslog.log = console.log.bind(window.console)
            conslog.debug = console.debug.bind(window.console);
            conslog.info = console.info.bind(window.console);
            conslog.warn = console.warn.bind(window.console);
            conslog.error = console.error.bind(window.console);
        } else if (partial == 4) {
            conslog.debug = console.debug.bind(window.console);
            conslog.info = console.info.bind(window.console);
            conslog.warn = console.warn.bind(window.console);
            conslog.error = console.error.bind(window.console);
        } else if (partial == 3) {
            conslog.info = console.info.bind(window.console);
            conslog.warn = console.warn.bind(window.console);
            conslog.error = console.error.bind(window.console);
        } else if (partial == 2) {
            conslog.warn = console.warn.bind(window.console);
            conslog.error = console.error.bind(window.console);
        } else if (partial == 1) {
            conslog.error = console.error.bind(window.console);
        } 
    }
}
/* ===================================================================== */

Handlebars.registerHelper('noop', function(options) {
    return options.fn(this);
});