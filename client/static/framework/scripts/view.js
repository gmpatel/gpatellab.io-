/* ===================================================================== */
class View {
    constructor(viewName, type) {
        var vt = type ? type : "view";
        this.view = { name: viewName, displayType: (type ? type : "view")};
    }
    
    navigate(json) {
        conslog.log("framework-log", ">>", "view.navigate()", "CLASS", json);
        if (this.viewNavigated && typeof(this.viewNavigated) == 'function') {
            conslog.log("framework-log", ">>", "this.viewNavigated", "calling...")
            this.viewNavigated(json);
            conslog.log("framework-log", ">>", "this.viewNavigated", "called...")
        }
        this.show(json);
    }

    /* show(view) >> Override this method to implement your own view specific navigation code! */
    show(json) {
        conslog.warn("framework-warn", ">>", "view.show()", "CLASS", "custom 'show()' view specific method not implemented in view '" + this.view.name + "', trigerred by abscract navigate() method!", json);
    }

    getViewName() {
        return this.view.name;
    }

    getDisplayType() {
        return this.view.displayType;
    }
}
/* ===================================================================== */