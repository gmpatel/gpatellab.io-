app.ready = function() {
    // Registering pages require authenticated access 
    app.registerPagesForAuthenticatedAccess([
        "home",
        "dashboard",
        "applications",
        "objects",
        "contact"
    ]);

    // Injecting topbar controls and menus 
    var topbarMenuViewName = "pg-app-vw-topbar-menu";
    var topbarMenuViewContainerId = "#topbar-container"; /* topbar container id "#topbar-container" is part of framework! */ 
    app.renderView (
        false,
        topbarMenuViewName, 
        topbarMenuViewContainerId,
        function() {
            // Do...
        }
    );
    
    // Injecting sidepane static controls and menus 
    var staticMenuViewName = "pg-app-vw-sidepane-static-menu";
    var staticMenuViewContainerId = "#pane-container"; /* sidepane container id "#pane-container" is part of framework! */
    app.renderView (
        false,
        staticMenuViewName, 
        staticMenuViewContainerId,
        function() {
            // Injecting sidepane dynamic controls and menus 
            var dynamicMenuViewName = "pg-app-vw-sidepane-dynamic-menu";
            var dynamicMenuViewContainerId = "#pane-container"; /* sidepane container id "#pane-container" is part of framework! */
            app.renderView (
                true,
                dynamicMenuViewName, 
                dynamicMenuViewContainerId,
                function() {
                    // Do...
                }
            );
        }
    );
};

app.pageNavigated = function(page, json) {
    conslog.log("app-log", ">>", "app.pageNavigated", page, json);
    if (page && page.name) {
        var allMenusSelector = "a[id^='pane-menu']";
        var targetMenuId = "#pane-menu" + "-" + page.name + (page.action ? "-" + page.action : "");
        var pageNameElementId = "#pg-name";
        var checkExist = setInterval(function() {
            if ($("#pn-menus-static").length) {
                $(allMenusSelector).removeClass("active");        
                $(targetMenuId).addClass("active");
                clearInterval(checkExist);
            } else {
                conslog.log("app-log", ">>", "app.pageNavigated", "waiting for the menus to be loaded!", page);
            }
         }, 100);
        $(pageNameElementId).text(page.name.toUpperCase());
        conslog.log("app-log", ">>", "app.pageNavigated", page, allMenusSelector, targetMenuId, pageNameElementId);
    }
}