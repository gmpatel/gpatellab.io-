# services/__init__.py

# system imports

# third-party imports

# local imports
from server.objects import db, ma

# package code
def init(webapp):
    db.init_app(webapp)
    ma.init_app(webapp)
    
    return