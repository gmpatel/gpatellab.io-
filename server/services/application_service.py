# routes/__init__.py

# system imports

# third-party imports
from flask import Flask, request, jsonify, render_template

#local imports
from server.objects import db, ma, application

# package code
def app_get_all():
    data = application.Application.query.all()
    return data

def app_get_id(id):
    data = application.Application.query.get(id)
    return data