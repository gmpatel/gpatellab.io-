# routes/__init__.py

# system imports

# third-party imports
from flask import Flask, request, jsonify, render_template

#local imports
from server.objects import db, ma, object

# package code
def obj_get_all():
    data = object.Object.query.all()
    return data

def obj_get_id(id):
    data = object.Object.query.get(id)
    return data