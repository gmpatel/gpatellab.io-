# app/__init__.py 

# system imports
import os

# third-party imports
from flask import Flask

# local imports
from server import config as cfg
from server import services
from server import routes

# package code
def create(config_name):
    webapp = Flask(
        __name__,
        instance_relative_config=True
    )
    webapp.config.from_object(cfg.app_config[config_name])
    
    services.init(webapp)
    routes.setup(webapp)
    
    return webapp