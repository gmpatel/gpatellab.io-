# config.py
import os

class Config(object):
    """
    Common configurations:
    """

    # Put any configurations here that are common across all environments
    SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://me:me123@/me?unix_socket=/cloudsql/gcp-wow-food-super-rtla-dev:asia-southeast1:ra-metadata-explorer'
    SQLALCHEMY_TRACK_MODIFICATIONS = False

class LocalConfig(Config):
    """
    Local configurations:
    """

    DEBUG = True
    SQLALCHEMY_ECHO = True
    SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://me:me123@35.247.183.220/me'

class DevelopmentConfig(Config):
    """
    Development configurations:
    """

    DEBUG = True
    SQLALCHEMY_ECHO = True
    SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://me:me123@/me?unix_socket=/cloudsql/gcp-wow-food-super-rtla-dev:asia-southeast1:ra-metadata-explorer'
    # SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://me:me123@35.247.183.220/me'

class ProductionConfig(Config):
    """
    Production configurations:
    """

    DEBUG = False
    SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://me:me123@/me?unix_socket=/cloudsql/gcp-wow-food-super-rtla-dev:asia-southeast1:ra-metadata-explorer'
    # SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://me:me123@35.247.183.220/me'

app_config = {
    'prod': ProductionConfig,
    'dev': DevelopmentConfig,
    'local': LocalConfig
}


# Equivalent URL:
# mysql+pymysql://<db_user>:<db_pass>@/<db_name>?unix_socket=/cloudsql/<cloud_sql_instance_name>