# routes/application_routes.py

# system imports

# third-party imports
from flask import request, jsonify

#local imports
from server.services import application_service
from server.objects import application

# package code
def include(webapp):
    @webapp.route('/api/applications', methods=["GET"])
    def app_get_all():
        data = application_service.app_get_all()
        return application.applications_schema.jsonify(data)

    @webapp.route('/api/applications/<id>', methods=["GET"])
    def app_get_id(id):
        data = application_service.app_get_id(id)
        return application.application_schema.jsonify(data)
