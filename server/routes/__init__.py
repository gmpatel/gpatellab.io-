# routes/__init__.py

# system imports

# third-party imports
from flask import request, render_template

# local imports
from . import application_routes, object_routes

# package code
def setup(webapp):
    @webapp.route("/")
    def home():
        return render_template("index.html")
    
    application_routes.include(webapp)
    object_routes.include(webapp)

    return webapp