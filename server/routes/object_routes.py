# routes/application_routes.py

# system imports

# third-party imports
from flask import request, jsonify

#local imports
from server.services import object_service
from server.objects import object

# package code
def include(webapp):
    @webapp.route('/api/objects', methods=["GET"])
    def obj_get_all():
        data = object_service.obj_get_all()
        return object.objects_schema.jsonify(data)

    @webapp.route('/api/objects/<id>', methods=["GET"])
    def obj_get_id(id):
        data = object_service.obj_get_id(id)
        return object.object_schema.jsonify(data)
