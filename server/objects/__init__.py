# app/__init__.py 

# system imports

# third-party imports
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow

# local imports

# package code
db = SQLAlchemy()
ma = Marshmallow()