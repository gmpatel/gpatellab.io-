# objects/application.py

# systme imports
from datetime import *

# third-party imports
from marshmallow import fields

# local imports
from . import db, ma, object

# package code
objects = db.Table('APPLICATION_OBJECTS_MAPPING',
    db.Column('APPLICATION_ID', db.Integer, db.ForeignKey('APPLICATIONS.ID'), primary_key=True),
    db.Column('OBJECT_ID', db.Integer, db.ForeignKey('OBJECTS.ID'), primary_key=True)
)

class Application(db.Model):
    __tablename__ = 'APPLICATIONS'

    id = db.Column('ID', db.Integer, primary_key = True, nullable=False)
    description = db.Column('DESCRIPTION', db.String(254), nullable=False)
    summary = db.Column('SUMMARY', db.String(1022), nullable=False)
    load_date_time = db.Column('load_date_time', db.DateTime, nullable=False, default=datetime.utcnow)
    publish_date_time = db.Column('publish_date_time', db.DateTime, nullable=False, default=datetime.utcnow)
    objects = db.relationship('Object', secondary=objects, lazy='subquery', backref=db.backref('APPLICATIONS', lazy=True))
    
    def __init__(self, description, summary):
        self.description = description
        self.summary = summary
        self.load_date_time = datetime.utcnow
        self.publish_date_time = datetime.utcnow

    def __repr__(self):
        return '<Application %s>' % self.description

class ApplicationSchema(ma.Schema):
    objects = fields.Nested('ObjectSchema', many=True)
    class Meta:
        # Fields to expose
        fields = ('id', 'description', 'objects', 'summary', 'load_date_time', 'publish_date_time')
        ordered = True

class ApplicationsSchema(ma.Schema):
    objects = fields.Nested('ObjectSchema', many=True, only='id')
    class Meta:
        # Fields to expose
        fields = ('id', 'description', 'objects', 'summary', 'load_date_time', 'publish_date_time')
        ordered = True

application_schema = ApplicationSchema()
applications_schema = ApplicationsSchema(many=True)