# objects/application.py

# systme imports
from datetime import *

# third-party imports
from marshmallow import fields

# local imports
from . import db, ma

# package code
objects = db.Table('OBJECTS_MAPPING',
    db.Column('SOURCE_OBJECT_ID', db.Integer, db.ForeignKey('OBJECTS.ID'), primary_key=True),
    db.Column('TARGET_OBJECT_ID', db.Integer, db.ForeignKey('OBJECTS.ID'), primary_key=True)
)

class Object(db.Model):
    __tablename__ = 'OBJECTS'

    id = db.Column('ID', db.Integer, primary_key = True, nullable=False)
    object_name = db.Column('OBJECT_NAME', db.String(254), nullable=False)
    object_desc = db.Column('OBJECT_DESC', db.String(254), nullable=False)
    system_code = db.Column('SYSTEM_CODE', db.String(254), nullable=False)
    schema_name = db.Column('SCHEMA_NAME', db.String(254), nullable=False)
    load_date_time = db.Column('load_date_time', db.DateTime, nullable=False, default=datetime.utcnow)
    objects = db.relationship('Object', secondary=objects, 
        primaryjoin=id==objects.c.SOURCE_OBJECT_ID, 
        secondaryjoin=id==objects.c.TARGET_OBJECT_ID, 
        lazy='subquery', 
        backref=db.backref('OBJECTS', lazy=True)
    )
    
    def __init__(self, object_name, object_desc, system_code, schema_name, load_date_time):
        self.object_name = object_name
        self.object_desc = object_desc
        self.system_code = system_code
        self.schema_name = schema_name
        self.load_date_time = datetime.utcnow

    def __repr__(self):
        return '<Object %s>' % self.object_name

class ObjectsSchema(ma.Schema):
    objects = fields.Nested('ObjectSchema', many=True, only='id')
    class Meta:
        # Fields to expose
        fields = ('id', 'object_name', 'object_desc', 'objects', 'system_code', 'schema_name', 'load_date_time')
        ordered = True

class ObjectSchema(ma.Schema):
    objects = fields.Nested('ObjectSchema', many=True)
    class Meta:
        # Fields to expose
        fields = ('id', 'object_name', 'object_desc', 'objects', 'system_code', 'schema_name', 'load_date_time')
        ordered = True

object_schema = ObjectSchema()
objects_schema = ObjectsSchema(many=True)